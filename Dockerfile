FROM alpine:latest
ADD HelloMe.class HelloMe.class
RUN apk --update add openjdk11-jre
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "HelloMe"]
